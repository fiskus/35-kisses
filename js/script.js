function toggleAbout() {
	var aboutBlock = document.getElementById("about");
	aboutBlock.getElementsByTagName("h2")[0].onclick = function() {
		if (aboutBlock.className.indexOf("open") > -1) {
			removeClass(aboutBlock, "open");
		} else {
			addClass(aboutBlock, "open");
		}
	}
}

function inputLabel() {
	var label = elm("q").parentNode.getElementsByTagName("label")[0];
	addEvent(elm("q"), "focus", function() {
		label.style.display = "none";
	});
	addEvent(elm("q"), "blur", function() {
		label.style.display = (elm("q").value == "") ? "block" : "none";
	});
}

var zoomIDs = {
	"scale-small": "small",
	"scale-normal": "normal",
	"scale-big": "big" 
}
var body = document.getElementsByTagName("body")[0];
function scaleText () {
	var scaleBlock = elm("scale");
	scaleBlock.innerHTML = "<ul><li id=\"scale-small\"><a href=\"javascript:void(0);\">A</a></li><li id=\"scale-normal\"><a href=\"javascript:void(0);\">A</a></li><li id=\"scale-big\"><a href=\"javascript:void(0);\">A</a></li></ul>";
	addClass(scaleBlock, "scale");
		for (var i in zoomIDs) {
			addEvent(elm(i), "click", zoom);
			addClass(elm(i), i);
		}
}
function zoom() {
	var scale = zoomIDs[this.id];
	for (var i in zoomIDs) {
		if (zoomIDs[i] == scale) {
			addClass(body, scale);
			addClass(elm(i), "active");
		} else {
			removeClass(body, zoomIDs[i]);
			removeClass(elm(i), "active");
		}
	}
	localStorage.setItem("fontSize", scale);
}

if (localStorage.getItem("fontSize")) {
	var fontSize = localStorage.getItem("fontSize");
	addClass(body, fontSize);
} else {
}

// var foo = localStorage.getItem("fontSize");
// localStorage.setItem("bar", foo);

DOMReady(function() {
	toggleAbout();
	inputLabel();
	scaleText();
});

